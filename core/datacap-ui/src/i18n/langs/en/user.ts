export default {
    common: {
        username: 'Username',
        password: 'Password',
        confirmPassword: 'Confirm Password',
        signin: 'Sign in',
        signup: 'Sign up',
        captcha: 'Captcha',
        sourceCount: 'Sources',
        queryCount: 'Queries',
        signout: 'Sign out',
        list: 'User List',
        assignAuthority: 'Assign Authority',
        assignRole: 'Assign Role'
    },
    auth: {
        signinTip: 'Enter your username and password to login',
        signupTip: 'Enter your username and password to sign up',
        usernameTip: 'Please enter your username',
        passwordTip: 'Please enter your password',
        confirmPasswordTip: 'Please confirm your password',
        captchaTip: 'Please enter the captcha',
        notUserTip: 'Don\'t have an account?',
        hasUserTip: 'Already have an account?',
        passwordNotMatchTip: 'Password does not match',
        usernameSizeTip: 'Username must be between 3 and 20 characters',
        passwordSizeTip: 'Password must be between 6 and 20 characters',
        captchaSizeTip: 'Captcha must be between 1 and 6 characters'
    },
    tip: {
        sourceCountTip: 'Statistics on the total number of access data sources',
        queryCountTip: 'Statistics on the total number of access queries',
        assignRoleSuccess: 'Role assignment succeeded'
    }
}
