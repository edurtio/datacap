import user from '@/i18n/langs/en/user'
import common from '@/i18n/langs/en/common'
import role from '@/i18n/langs/zhCn/role'
import schedule from '@/i18n/langs/en/schedule'
import dashboard from '@/i18n/langs/en/dashboard'
import dataset from '@/i18n/langs/en/dataset'
import state from '@/i18n/langs/en/state'
import query from '@/i18n/langs/en/query'
import source from '@/i18n/langs/en/source'
import grid from '@/i18n/langs/en/grid'

export default {
    common: common,
    user: user,
    role: role,
    schedule: schedule,
    dashboard: dashboard,
    dataset: dataset,
    state: state,
    query: query,
    source: source,
    grid: grid
}
