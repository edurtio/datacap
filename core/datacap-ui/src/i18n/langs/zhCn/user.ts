export default {
    common: {
        username: '用户名',
        password: '密码',
        confirmPassword: '确认密码',
        signin: '登录',
        signup: '注册',
        captcha: '验证码',
        sourceCount: '数据源',
        queryCount: '查询',
        signout: '退出登录',
        list: '用户列表',
        assignAuthority: '分配权限',
        assignRole: '分配路由'
    },
    auth: {
        signinTip: '请输入用户名和密码登录',
        signupTip: '请输入用户名和密码注册',
        usernameTip: '请输入用户名',
        passwordTip: '请输入密码',
        confirmPasswordTip: '请输入确认密码',
        captchaTip: '请输入验证码',
        notUserTip: '没有账号?',
        hasUserTip: '已有账号?',
        passwordNotMatchTip: '密码不匹配',
        usernameSizeTip: '用户名必须在3-20个字符之间',
        passwordSizeTip: '密码必须在6-20个字符之间',
        captchaSizeTip: '验证码必须在1-6个字符之间'
    },
    tip: {
        sourceCountTip: '创建数据源总数统计',
        queryCountTip: '访问查询总数统计',
        assignRoleSuccess: '分配路由成功',
    }
}
